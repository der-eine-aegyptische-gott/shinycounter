import { StatusBar } from 'expo-status-bar';
import React, {useState,useEffect} from 'react';
import { StyleSheet, Text, View, Button, TextInput, ScrollView, SafeAreaView } from 'react-native';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("db.shinycounter100");

const initDB = function(){
  db.transaction((tx)=>{
    tx.executeSql('CREATE TABLE IF NOT EXISTS Counter (id INTEGER UNIQUE, counter INTEGER)', [], (tx, succes) => {
      console.log(succes);
    }, (tx, error) => {
      console.log(error);
    });
    tx.executeSql('INSERT OR IGNORE INTO Counter(id,counter) VALUES(1,0)', [], (tx, succes) => {
      console.log(succes);
    }, (tx, error) => {
      console.log(error);
    });
    tx.executeSql('CREATE TABLE IF NOT EXISTS PokemonList (name TEXT,attempts INTEGER)', [], (tx, succes) => {
      console.log(succes);
    }, (tx, error) => {
      console.log(error);
    });
  });
}

export default function App() {
  const [counter, setCounter] = useState(0);
  const [pokemon_name, setPokemonName] = useState(null);
  const [pokemon_list, setPokemonList] = useState([]);

  useEffect(() => {
    initDB();
    getCounter();
    getList();
    //truncateDatabase();
  },[]);

  const truncateDatabase = function(){
    db.transaction((tx)=>{
      tx.executeSql("TRUNECATE TABLE Counter", [], (tx, succes) => {
        console.log(succes);
      }, (tx, error) => {
        console.log(error);
      });
      tx.executeSql("TRUNECATE TABLE PokemonList", [], (tx, succes) => {
        console.log(succes);
      }, (tx, error) => {
        console.log(error);
      });
    });
  }

  const getList = function(){
    db.transaction((tx)=>{
    tx.executeSql("SELECT * FROM PokemonList",
        [],
        (_,{rows})=>{
          setPokemonList(rows._array)
        });
    });
  }

  const getCounter = function(){
    db.transaction((tx)=> {
      tx.executeSql("SELECT * FROM Counter",
          [],
          (_, {rows}) => {
            setCounter(rows.item(0).counter);
          });
    });
  }

  const increaseCounter = function(){
    const newVal = counter+1;
    console.log("set counter+")
    db.transaction((tx)=>{
      tx.executeSql("UPDATE Counter SET counter = ? WHERE id = 1",[newVal], (tx, succes) => {
        console.log(succes);
      }, (tx, error) => {
        console.log(error);
      });
    });
    setCounter(newVal);
    console.log("insert counter+")
  }

  const reduceCounter = function(){
    let newVal = counter;
    if(counter > 0){
      newVal--
    }
    db.transaction((tx)=>{
      tx.executeSql("UPDATE Counter SET counter = ? WHERE id = 1",[newVal], (tx, succes) => {
        console.log(succes);
      }, (tx, error) => {
        console.log(error);
      });
    });
    setCounter(newVal);
    console.log("insert counter-")
  }

  const pokemonValue = function(text){
    setPokemonName(text);
  }

  const savePokemon = function(){
    db.transaction((tx)=>{
      tx.executeSql("INSERT INTO PokemonList (name, attempts) VALUES (?, ?)",[pokemon_name,counter], (tx, succes) => {
        console.log(succes);
      }, (tx, error) => {
        console.log(error);
      });
      tx.executeSql("SELECT * FROM PokemonList",
          [],
          (_,{rows})=>{
        setPokemonList(rows._array)
      });
    });
    db.transaction((tx)=>{
      tx.executeSql("UPDATE Counter SET counter = 0 WHERE id = 1",[], (tx, succes) => {
        console.log(succes);
      }, (tx, error) => {
        console.log(error);
      });
    });
    setCounter(0);
  }

  const renderPokemonList = function(){
    return pokemon_list.map((pokemon,index) => <View style={styles.listitem} key={index}>
      <Text style={[{flexGrow: 1}]}>{pokemon.name}</Text>
      <View style={styles.attempts}>
        <Text>{pokemon.attempts}</Text>
      </View>
    </View>)
  }

  return (
    <View style={styles.container}>
      <View style={styles.buttonRow}>
        <View style={styles.button}>
          <Button onPress={() => reduceCounter()} title="-"/>
        </View>
        <View style={styles.button}>
          <Button onPress={() => increaseCounter()} title="+"/>
        </View>
      </View>
      <Text style={styles.text}>Finished Raids: {counter}</Text>
      <View style={styles.inputContainer}>
        <TextInput placeholder='your pokemon name' style={styles.input} onChangeText={(text) => {pokemonValue(text)}} value={pokemon_name}/>
        <View style={[{justifyContent:'center'}]}>
          <Button onPress={() => {savePokemon()}} title="Catched!"/>
        </View>
      </View>
      <StatusBar style="auto" />
      <SafeAreaView style={styles.safeAreaContainer}>
        <View style={[{flexDirection:'row', marginBottom:10}]}>
          <Text style={[{flexGrow:1,fontWeight:'bold'}]}>Name</Text>
          <Text style={[{fontWeight:'bold'}]}>Attempts</Text>
        </View>
        <ScrollView>
          {renderPokemonList()}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 45,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  text: {
    marginTop: 5,
    fontSize: 25,
  },
  buttonRow: {
    flexDirection: 'row',
  },
  button: {
    width: 75,
    margin: 10,
  },
  input: {
    borderColor: 'black',
    borderWidth: 2,
    padding: 5,
    marginRight: 20,
  },
  inputContainer: {
    flexWrap: 'nowrap',
    flexDirection: 'row',
    marginTop: 10,
    padding: 20,
  },
  listitem: {
    flexDirection: 'row',
  },
  safeAreaContainer: {
    marginTop: 10,
    width: 300,
    marginBottom: 10,
  },
  attempts: {
  },
});
